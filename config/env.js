
var devConfiguration = {
  port: process.env.PORT || 3000,
  dbConnectionString: 'mongodb://localhost:27017/strider',
  cdnUri: 'http://b3014a5c447801daf159-aab1b1665c18d03bbbf5846be888e985.r49.cf4.rackcdn.com',
};
exports.config = function () {
  return devConfiguration;
};
