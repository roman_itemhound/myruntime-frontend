'use strict';

var passport = require('passport'),
  request = require('request');

module.exports = function() {

  //serialize sessions
  passport.serializeUser(function(userData, done){
    done(null, userData.Model._id);
  });

  passport.deserializeUser(function(userId, done){
    var getURL = 'http://121.97.206.36:9090/api/user/' + userId; 
    request.get(getURL, function(err, response, body){
      done(err, body);
    });    
  });
};