'use strict';

module.exports = function(grunt) {

  var watchFiles = {    
    //serverJS: ['Gruntfile.js', 'server.js', 'config/*.js', 'app/**/*.js'],
    //clientJS: ['public/mainApp/js/**/*.js','public/adminApp/js/**/*.js'],
    serverJS: ['server.js', 'config/*.js', 'app/**/*.js'],
    clientJS: [],
    clientCSS: ['public/css/*.css'],
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),    
    watch:{
      serverJS: {
        files: watchFiles.serverJS,
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      clientJS: {
        files: watchFiles.clientJS,
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      clientCSS: {
        files: watchFiles.clientCSS,
        tasks: ['csslint'],
        options: {
          livereload: true
        }
      }
    },
    csslint: {
      options: {
        csslintrc: '.csslintrc',
      },
      all: {
        src: watchFiles.clientCSS
      }
    },
    jshint: {
      all: {
        src: watchFiles.clientJS.concat(watchFiles.serverJS),
        options: {
          jshintrc: '.jshintrc'
        }
      }
    },
    nodemon: {
      dev: {
        script: 'server.js',
        options: {
          nodeArgs: ['--debug'],
          ext: 'js,html',
          watch: watchFiles.serverJS
        }
      }
    },
    concurrent: {
      default: ['nodemon', 'watch'],      
      options: {
        logConcurrentOutput: true
      }
    },
    ngAnnotate: {
      production: {
        files: {
          'public/dist/application.js': '<%= applicationJavaScriptFiles %>',
          'public/dist/adminApplication.js': '<%= adminApplicationJavaScriptFiles %>'
        }
      }
    },
    uglify: {
      production: {
        options: {
          mangle: false
        },
        files: {
          'public/dist/application.min.js': 'public/dist/application.js',
          'public/dist/adminApplication.min.js': 'public/dist/adminApplication.js'
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          'public/dist/application.min.css': '<%= applicationCSSFiles %>',
        }
      }
    }
  });
  
  // Load NPM tasks 
  require('load-grunt-tasks')(grunt);

  grunt.task.registerTask('loadConfig', 'Task that loads the config into a grunt option.', function() {    
    var config = require('./config/all.js');
    grunt.config.set('applicationJavaScriptFiles', config.assets.appjs);
    grunt.config.set('adminApplicationJavaScriptFiles', config.assets.adminjs);
    grunt.config.set('applicationCSSFiles', config.assets.css);
  });

  //grunt.registerTask('default', ['lint', 'concurrent:default']);
  grunt.registerTask('default', ['build']);
  //grunt.registerTask('lint', ['jshint', 'csslint']);
  grunt.registerTask('lint', ['jshint']);
  //grunt.registerTask('build', ['lint', 'loadConfig', 'ngAnnotate', 'uglify', 'cssmin']);
  grunt.registerTask('build', ['lint', 'loadConfig', 'ngAnnotate', 'uglify']);

};

