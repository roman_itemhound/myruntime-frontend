'use strict';

app.controller('accountSettingsCtrl', function ($scope, $http, $location){
  
  $http.get('/api/user/').success(function(data){ 
    if(!data){
      $location.path('login');
    }    
    else{
      $http.get('/api/user/' + data.userId).success(function(user){
        $scope.user = user;
      });
    }
  });

  $scope.activateAccount = function(){
     $http({
      method: 'POST',
      url: '/api/activateUserAccount',
      data:{ activated: false, userId: $scope.user.Model._id}
    }).success(function (data, status, headers, config){
      console.log(data);
    });
  };

});