'use strict';

app.controller('raceCtrl', function ($scope, $http, $routeParams, $location){
  
  var url = 'http://121.97.206.36:9090';
  //$scope.store  = storeService.store;

  $scope.searchBib = function(bibNumber){    
    var location = "runner/" + $scope.race._id + "/" + bibNumber;    
    $location.path(location);    
  }

  $scope.resultsOverdue = false;  

  $http.get( url + '/api/race/nick/'+$routeParams.raceNick).success(function(data){

    data.raceDate = (new Date(data.raceDate));
    var dateToday = new Date();

    if(dateToday > data.raceDate){
      $scope.resultsOverdue = true;
    }

    $scope.race = data;
  }).error(function(data){
    $location.path("pageNotFound");
  });
;});