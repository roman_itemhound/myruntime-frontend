'use strict';

app.controller('adminLoginCtrl', function ($scope, $http,$window){      

  $scope.signin = function(){
    $scope.loading = true;
    $scope.error = undefined;
    
    $http.post('/auth/adminlogin', $scope.credentials).success(function(response) {
      $scope.loading = false;
      $window.location.href = '/admin';
    }).error(function(response){
      $scope.loading = false;
      $scope.error = response;     
    });
  };
    
});
