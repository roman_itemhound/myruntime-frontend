'use strict';

app.controller('loginCtrl', function ($scope, $window, $http){      
  
  $scope.register = function(){
    $window.location.href = '#/signup';
  };

  $scope.signin = function(){
    $scope.loading = true;
    $scope.error = undefined;
    
    $http.post('/auth/login', $scope.credentials).success(function(response) {
      $scope.loading = false;
      $window.location.href = '/me';
    }).error(function(response){
      $scope.loading = false;
      $scope.error = response;     
    });
  };

});

app.controller('registerCtrl', function ($scope, $http, $window){
  $scope.genders = [
    {"name":"Male", "value":"M"},
    {"name": "Female", "value": "F"}
  ];

  $scope.loading = false;
  
  $scope.register = function(){
    var postData = $scope.data;
    $scope.loading = true;
    
    $http.post('/auth/register', postData).
    success(function(response){
      $scope.loading = false;
      $scope.showMeModal();
    }).error(function(response){
      $scope.loading = false;
      alert(response);
    });
  }

  $scope.showMeModal = function(){
    $('.basic.modal')
      .modal('setting', {
        onHide: function(){
          $window.location.href = '/';
        }
      })
      .modal('show');
  };

});
