'use strict';

app.controller('connectCtrl', function ($scope, $http, $routeParams, $route){
  var url = 'http://121.97.206.36:9090';
    
  $http.get('/api/user/').success(function(data){  
    $scope.user = data;
    if(data){
      $scope.userLoggedIn = true;
    }
    else{
     $scope.userLoggedIn = false; 
    }  
  });

  $scope.validateActivation = function(){    
    $http({
      method: 'POST',
      url: url + '/api/validateBib',
      data:{ bibnumber: $scope.bibNumber, activationCode: $scope.activationCode, userId: $scope.user.Model._id}
    }).success(function (data, status, headers, config){
      alert("Activated Bib!");
      $scope.bibNumber = '';
      $scope.activationCode = '';

      //

      //TODO
      //modal share on fb;
    }).error(function(data, status, headers, config){
      console.log(data);
      alert("Error in validation: \r\n" + data);
    })

  }
;});