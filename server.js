/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var config = require('./config/env').config();

// Bootstrap db connection
var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  console.log("Setting rackspace cdn...");  
  console.log("Successfuly set CDN bucket");

  var app = require('./config/express')(db, config.cdnUri);

  //Bootstrap passport config
  require('./config/passport')();

  // Start the app by listening on <port>
  app.listen(config.port, function(){
    process.env.NODE_ENV = 'development';
    console.log('Running on NODE_ENV: %s', process.env.NODE_ENV);
    console.log('Express server listening on port ' + config.port);
  });

  // Expose app
  exports = module.exports = app;

});      
