var request = require('request');

module.exports = function (app) {

  app.get('/api/user', function(req, res){    
    res.send(req.user);
  });

  app.get('/auth/facebook', function(req, res){
    request.get('http://121.97.206.36:9090/api/facebook/695423216', function(err, response, body){
      if(err){
        res.send(err);
      }
      else{
        var user = JSON.parse(body);
        req.login(user, function(err){
          if(err){
            console.log(err);
            res.redirect('/#/login');
          }
          else{
            res.redirect('/#/me');
          }
        });
      }
    });
  });

  app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });
};